﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script_grappleHookObj : MonoBehaviour
{
    public Script_PlayerController player;
    public Vector2 m_initialPosition;
    public Rigidbody2D rigid;
    public float speed = 2f;
    public bool isMovingUp = true;
    public float boundingDistance = 5f;
    // Start is called before the first frame update
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Script_PlayerController>();
        rigid = this.GetComponent<Rigidbody2D>();
        m_initialPosition = new Vector2(transform.position.x, transform.position.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
            player.grappleUnlocked = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (isMovingUp)
        {
            if (this.transform.position.y > (m_initialPosition.y + boundingDistance))
            {
                isMovingUp = false;
            }
            rigid.velocity = new Vector2(0, speed);
        }
        else if (!isMovingUp)
        {
            if (this.transform.position.y < (m_initialPosition.y - boundingDistance))
            {
                isMovingUp = true;
            }
            rigid.velocity = new Vector2(0, -speed);
        }
    }
}
