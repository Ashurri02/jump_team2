﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script_BasicEnemy : MonoBehaviour
{
    public Vector2 m_initialPosition;

    public SpriteRenderer sprite;
    private Script_PlayerController m_player;
    private Rigidbody2D m_rigid;

    public bool isMovingRight = true;
    public float boundingDistance = 100f;
    public float m_speed = 20f;
    // Start is called before the first frame update
    private void Awake()
    {
        m_initialPosition = new Vector2(transform.position.x, transform.position.y);
        m_player = GameObject.FindGameObjectWithTag("Player").GetComponent<Script_PlayerController>();
        m_rigid = this.GetComponent<Rigidbody2D>();
        sprite = this.GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Colliding");
            //Damage Player
        }
    }


    public void proccessMovement()
    {
        if (isMovingRight)
        {
            if (this.transform.position.x > (m_initialPosition.x + boundingDistance))
            {
                isMovingRight = false;
                sprite.flipX = true;
                
            }
            m_rigid.velocity = new Vector2(m_speed, 0);
        }
        else if(!isMovingRight)
        {
            if (this.transform.position.x < (m_initialPosition.x - boundingDistance))
            {
                isMovingRight = true;
                sprite.flipX = false;
            }
            m_rigid.velocity = new Vector2(-m_speed, 0);
        }
    }
    // Update is called once per frame
    void Update()
    {
        proccessMovement();
    }
}
