﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_PlayerController : MonoBehaviour
{
    [Header("Sprites")]
    public Sprite idle;
    public Sprite JumpUp;
    public Sprite JumpDown;
    public Sprite Grapple;
    public Sprite Running;

    [Header("Components")]
    public Camera playerCamera;
    private Rigidbody2D m_rigid;
    private SpriteRenderer m_spriteRenderer;

    public bool grappleUnlocked = false;
    public bool canJump = false;
   
    //Components For Grappling
    [Header("Grapple Components")]
    private LineRenderer m_lineRenderer;
    private TargetJoint2D newjoint;
    public GameObject playerFeet;
    public float radius = 15.0f;
    public float force = 100.0f;
    public bool isgrappling = false;
    public Vector2 point;
    /*
     * tried to make a mario style jump and movment 
     */
    [Header("Movement")]
    [Tooltip("The speed the rigidbody moves")]
    //Movement - Jumping

    public float currentJumpforce = 10f;
   
    public int maxJump = 1;
    public int currJump = 0;
    //Movement - Left Right
    public const float fAcceleration = 20.0f;
    public const float Deceleration = 10.0f;
    public const float maxVelocity = 15.0f;
    public float currentVelcoity = 0.0f;
   

    [Header("GrappableObjects")]
    //Gets a count of all the grappable objects
    public GameObject[] getCount;
    //Returns whether current object is grappable or not
    bool grappable = false;

    private bool isGrappable()
    {
        //Get a count of all the gameobjects
        getCount = GameObject.FindGameObjectsWithTag("grappable");
        //Iterate through all the gameobjects and see if they are grappable
        for (int i = 0; i < getCount.Length; i++)
        {
            if (getCount[i].GetComponent<GrappleObject>().canGrapple == true)
            {
                //If the current object is grappable break the loop and return true
                grappable = true;
                break;
            }
            else
            {
                grappable = false;
            }
        }
        if (grappable == true)
        {
            return (true);
        }
        else
        {
            return (false);
        }
    }
    //Variables
    private enum animationState { 
        isJumping,
        isFalling,
        isMoving,
        isIdle,
        isGrappling
 
    };

    animationState aniState;
    private void Awake()
    {
        m_lineRenderer = gameObject.GetComponent<LineRenderer>();
        m_rigid = gameObject.GetComponent<Rigidbody2D>();
        m_spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        m_lineRenderer.enabled = false;
        //playerFeet = GameObject.
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, radius);
    }
    //If there are any other keyboard inputs
    //Put them in here.
    private void ProcessInputKeyBoard()
    {
        //Move Left
        if (Input.GetKey(KeyCode.A) && !isgrappling)
        {
            if (currentVelcoity >= -maxVelocity)
            {
                //start acceleration
                currentVelcoity += -fAcceleration * Time.deltaTime;
            }
            aniState = animationState.isMoving;
            m_spriteRenderer.flipX = true;
            
            m_rigid.velocity = new Vector2(currentVelcoity, m_rigid.velocity.y);
        }
        else
        {
            //if current velocity isnt 0 decelerate over time
            if (currentVelcoity < 0f)
            {
                currentVelcoity += Deceleration * Time.deltaTime;
            }
        }

        //Move Right
        if (Input.GetKey(KeyCode.D) && !isgrappling)
        {
            if (currentVelcoity <= maxVelocity)
            {
                //start acceleration
                currentVelcoity += fAcceleration * Time.deltaTime;
            }
            aniState = animationState.isMoving;
            m_spriteRenderer.flipX = false;
          
            m_rigid.velocity = new Vector2(currentVelcoity, m_rigid.velocity.y);
        }
        else
        {
            //if current velocity isnt 0 decelerate over time
            if (currentVelcoity > 0f)
            {
                currentVelcoity += -Deceleration * Time.deltaTime;
            }
        }

        //Jump
        if (Input.GetKeyDown(KeyCode.Space) && currJump < maxJump)
        {
            //if (currentJumpforce <= maxJumpHeight && currentJumpforce >= 0)
            //{
            //    currentJumpforce += -jumpAcceleration * Time.deltaTime;
            //}

            aniState = animationState.isJumping;
            m_rigid.velocity = new Vector2(currentVelcoity, currentJumpforce);
        }

    }

    private void stateCheck()
    {
        float fYAcceleration = m_rigid.velocity.y;
        float fXAcceleration = m_rigid.velocity.x;

        if (fYAcceleration < fYAcceleration--)
        {
            aniState = animationState.isFalling;
        }
    }
    private void animationCheck()
    {
        switch (aniState)
        {
            case animationState.isFalling:
                m_spriteRenderer.sprite = JumpDown;

                break;
            case animationState.isGrappling:
                m_spriteRenderer.sprite = Grapple;
                break;
            case animationState.isMoving:
                m_spriteRenderer.sprite = idle;
                break;
            case animationState.isJumping:
                m_spriteRenderer.sprite = JumpUp;
                break;
            case animationState.isIdle:
                m_spriteRenderer.sprite = idle;
                break;

        }
    }
    
    //This is Mainly for grappling, but if there are any inputs for Mouse
    //Put them here.
    private void ProcessInputMouse()
    {
        Vector2 mousePos = (Vector2)playerCamera.ScreenToWorldPoint(Input.mousePosition);

        if (grappleUnlocked)
        { 
            if (Input.GetMouseButtonDown(0) && isGrappable())
            {
                point = mousePos;
                isgrappling = true;
                newjoint = this.gameObject.AddComponent<TargetJoint2D>();
                newjoint.target = point;
                newjoint.maxForce = force;
            
            }
            if (new Vector2(transform.position.x, transform.position.y) == point)
            {
                isgrappling = false;
            //m_lineRenderer.SetPosition(0, point);
            //m_lineRenderer.SetPosition(1, transform.position);
                m_rigid.gravityScale = 2;
                m_lineRenderer.enabled = false;
                newjoint.enabled = false;
                Destroy(newjoint);

            }
            if (Input.GetKey(KeyCode.Mouse0) && isGrappable())
            {
                Debug.Log("grappling");   
                aniState = animationState.isGrappling;
            //transform.localPosition = Vector3.MoveTowards(transform.position, point, grappleSpeed * Time.deltaTime);
                m_rigid.gravityScale = 0;

                m_lineRenderer.enabled = true;
                m_lineRenderer.SetPosition(1, point);
                m_lineRenderer.SetPosition(0, transform.position);
            }
            else if (Input.GetKeyUp(KeyCode.Mouse0))
            {
           
            
                m_rigid.gravityScale = 2;
                newjoint.enabled = false;
                m_lineRenderer.enabled = false;
                Destroy(newjoint);
            }
        }
      

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == gameObject.layer)
        {
            isgrappling = false;
        }
    }
    // Update is called once per frame
    void Update()
    {
        animationCheck();
        ProcessInputMouse();
        ProcessInputKeyBoard();
    }
}
