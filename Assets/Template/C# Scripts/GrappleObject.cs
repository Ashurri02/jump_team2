﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleObject : MonoBehaviour
{
    [Header("GrappableObject Variables")]
    public GameObject grappableObject;
    public bool mouseOver = false;
    public bool canGrapple = false;

    //Checks if the mouse is over the object
    public void OnMouseEnter()
    {
        Debug.Log("mouse enter");
        mouseOver = true;
    }

    //Checks if the mouse is not on the object
    public void OnMouseExit()
    {
        Debug.Log("mouse exit");
        mouseOver = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Initialize variables
        grappableObject = this.GetComponent<GameObject>();
        mouseOver = false;
        canGrapple = false;
    }

    void Update()
    {
        //Updates and changes canGrapple to true if mouse is over grappable object, else it equals false
        if (mouseOver == true)
        {
            canGrapple = true;
        }
        else
        {
            canGrapple = false;
        }

    }


    
}

