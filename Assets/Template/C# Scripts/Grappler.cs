﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grappler : MonoBehaviour
{
	public Camera mainCamera;
	public LineRenderer lineRenderer;
	public DistanceJoint2D distanceJoint;
	public Script_PlayerController GetPlayer;

	[Header("GrappableObjects")]
	//Gets a count of all the grappable objects
	public GameObject[] getCount;
	//Returns whether current object is grappable or not
	bool grappable = false;

	// Use this for initialization
	void Start()
	{
		distanceJoint.enabled = false;
	}

	bool isGrappable()
	{
		//Get a count of all the gameobjects
		getCount = GameObject.FindGameObjectsWithTag("grappable");
		//Iterate through all the gameobjects and see if they are grappable
		for (int i = 0; i < getCount.Length; i++)
		{
			if (getCount[i].GetComponent<GrappleObject>().canGrapple == true)
			{
				//If the current object is grappable break the loop and return true
				grappable = true;
				break;
			}
			else
			{
				grappable = false;
			}
		}
		if (grappable == true)
		{
			return (true);
		}
		else
		{
			return (false);
		}
	}

	// Update is called once per frame
	void Update()
	{
		if (GetPlayer.grappleUnlocked == true)
		{
			
			if (Input.GetKeyDown(KeyCode.Mouse0) && isGrappable() == true)
			{

					Vector2 mousePos = (Vector2)mainCamera.ScreenToWorldPoint(Input.mousePosition);
					distanceJoint.connectedAnchor = mousePos;
					distanceJoint.enabled = true;
					distanceJoint.distance = 0;

					lineRenderer.enabled = true;
					lineRenderer.SetPosition(0, mousePos);
					lineRenderer.SetPosition(1, transform.position);
				

			}
			else if (Input.GetKeyUp(KeyCode.Mouse0))
			{
				distanceJoint.enabled = false;
				lineRenderer.enabled = false;
			}

			if (distanceJoint.enabled)
			{
				lineRenderer.SetPosition(1, transform.position);
			}
		}

	}

}



